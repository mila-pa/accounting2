package com.company;

/**
 * Created by panteyenko on 12/19/2016.
 */
public class Contract extends Employee {
int partI;
int partII;
    double percent;
    int base;

    public Contract(String lastName, String firstName, String position, double percent, int base) {
        super(lastName, firstName, position);
        this.partI = 50000;
        this.partII = 65000;
        this.percent = percent;
        this.base = base;
    }

    public double getSalary() {
        double managerSalary = (this.partI + this.partII) * this.percent + this.base;

        System.out.println("Manager Salary per Month = " + managerSalary);

        return managerSalary;

    }
}
