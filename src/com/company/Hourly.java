package com.company;

/**
 * Created by panteyenko on 12/19/2016.
 */
public class Hourly extends Employee implements Calculate {

  int rate;
  int hour;

    public Hourly(String lastName, String firstName, String position, int rate, int hour) {
        super(lastName, firstName, position);
        this.rate = rate;
        this.hour = hour;
    }
    public double getSalary() {
        double course = 26 + (Math.random() * (0.9 - 0.1) + 0.1);
        double perHour = this.rate * course; // per hour
        double designerSalary = perHour * this.hour;
        System.out.println("Designer Salary per Month = " + designerSalary);

        return designerSalary;


    }
}
