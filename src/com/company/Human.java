package com.company;

/**
 * Created by panteyenko on 12/19/2016.
 */
public class Human {
    String lastName;
    String firstName;


    public Human(String lastName, String firstName) {
        this.lastName = lastName;
        this.firstName = firstName;

    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getFirstName() {
        return firstName;
    }
}
