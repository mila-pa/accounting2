package com.company;

public class Main {

    public static void main(String[] args) {
        Employee[] employees = new Employee[4];

        employees[0] = new Wage("Ivanova", "Helen", "Assistant", 4500);
        employees[1] = new Hourly("Vakulenko", "Dima", "Designer", 7, 164);
        employees[2] = new Contract("Korenkova", "Hanna", "Sales ManagerI", 0.05, 0);
        employees[3] = new Contract("Petrova", "Tatiana", "Sales ManagerII", 0.03, 1000);

//        Employee temp = employees[1];
//        for (Employee employee : employees) {
//            if (employee instanceof Designer) {
//                Designer designer = (Designer) employee;
//
//                System.out.println(designer.getSalary());
//            }
//        }

        for (Employee employee : employees) {
            System.out.print(employee.lastName + "\t" + employee.firstName + "\t" + employee.position);
            System.out.println();
            employee.getSalary();
            //    System.out.println(employee.getSalary());
        }


    }
}
