package com.company;

/**
 * Created by panteyenko on 12/19/2016.
 */
public class Wage extends Employee implements Calculate {
    int perMonth;

    public Wage(String lastName, String firstName, String position, int perMonth) {
        super(lastName, firstName, position);
        this.perMonth = perMonth;
    }
    public double getSalary() {


        System.out.println("Assistant Salary per Month = " + this.perMonth);
        return this.perMonth;

    }

}
